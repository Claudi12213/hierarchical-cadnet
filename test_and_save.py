import os
from hierarchical_cadnet.network_edge import HierarchicalGCNN as HierGCNN
from hierarchical_cadnet.test_and_save_utils import create_hier_graphs, test_step, write_step_wth_prediction
import numpy as np
import tensorflow as tf

if __name__ == "__main__":
    with_labels = True
    step_dir = "data/"
    step_name = "1_true"
    checkpoint_path = "checkpoint/MF_CAD++_residual_lvl_7_edge_MFCAD++_units_512_date_2021-07-27_epochs_100.ckpt"
    num_classes = 25
    num_layers = 7
    units = 512
    dropout_rate = 0.3

    model = HierGCNN(units=units, rate=dropout_rate, num_classes=num_classes, num_layers=num_layers)
    loss_fn = tf.keras.losses.CategoricalCrossentropy()

    model.load_weights(checkpoint_path)

    graph, shape, labels = create_hier_graphs(os.path.join(step_dir, f"{step_name}.step"), with_labels=with_labels)
    y_pred = test_step(graph)
    write_step_wth_prediction(os.path.join(step_dir, f"{step_name}_pred.step"), shape, y_pred)

    if with_labels:
        labels = np.array(labels)
        print(f"Predictions: {y_pred}")
        print(f"True labels: {labels}")

        print(f"Acc: {np.sum(np.where(y_pred == labels, 1, 0)) / labels.shape[0]}")
